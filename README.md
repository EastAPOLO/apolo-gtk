# apolo-gtk

A simple GTK3 theme built for my own use to avoid dealing with complex CSS
and preprocessors I don't understand. Largely incomplete, expect breakage.
There are no image assets currently.

## Install

In the repo's root directory run:

```
make install
```

## Customize

Colors are defined at the end of gtk.css using GTK's `@define-color`. Here are
some of the main colors:

* #802626 — accents
* #DADADA — foreground text
* #1C1C1C — primary background color
* #1A1717 — secondary (darker) background color
* #2B2B2B — background color used for various input boxes and buttons
* #242424 — button hover color
